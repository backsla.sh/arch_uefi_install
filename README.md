# Arch Linux for UEFI
I have tried many guides for installing Arch Linux from the internet but most of them lack a few steps due to which installation may not succeed. One such issue I faced was, not creating the EFI partition. This guide will hopefully help you install Arch Linux cleanly without any errors. If you face any, please let me know.

Note: This guide assumes that you have succesfully booted into a live USB partition and you're currently looking at the terminal.

## Step 1: Partitioning the disks
The first step includes partitioning your harddisk/eMMC. In my case my laptop has 32 Gb of eMMC storage which is repesented as `/dev/mmcblk0`, if you have a harddisk then its probably represented as `/dev/sda`. Use `lsblk` to list all the drives and partitions. I'm using cfdisk to perform a disk partition layout. I strongly recommend using cfdisk for it's simplicity in use.

For a basic partition layout table, use the following structure.
* Root partition (`/dev/sda1` or `/dev/mmcblk0p1`) 
* Swap partition (`/dev/sda2` or `/dev/mmcblk0p2`)
* EFI partition (`/dev/sda3` or `/dev/mmcblk0p3`) 

Now let's actually start creating disk layout partition table by running cfdisk command against your harddisk/eMMC `cfdisk  <storage>`, select `dos` label type, then use the following configuration: New->Size: 300M and press enter key, select Type from the bottom menu and choose EFI System partition type, as shown in the following screenshots.

Next, let's create the swap partition using the same procedure. Use down arrow key and select again the remaining Free space and repeat the configuration: New-> partition-size = 2xRAM and press enter key, select Type as Linux swap.

Finally for the root partition use the following configuration: New-> partition-size = rest of free space and press enter key, select Type as Linux filesystem.

After you review partition table, select write and answer with yes to apply changes and then type quit to exit cfdisk utility as shown in the below images.

